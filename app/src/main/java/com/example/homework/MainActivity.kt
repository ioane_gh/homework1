package com.example.homework

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        numberGenerator.setOnClickListener {
            val number = (-100..100).random()
            d("random_number", "this is your random number: $number")
            dividedByFive(number)
        }
    }

    private fun dividedByFive(number:Int) = if (number % 5 == 0 && number / 5 > 0){
        mainText.text = "YES"
    }else{
        mainText.text = "NO"
    }
}